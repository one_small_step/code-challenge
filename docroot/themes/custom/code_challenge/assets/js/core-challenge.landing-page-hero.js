
var hero = document.getElementsByClassName('landing-page-hero');
if (typeof(hero) != 'undefined' && hero != null){
  if (hero[0].getAttribute('data-hero-image') !== "undefined") {
    var hero_region = document.getElementsByClassName('usa-hero');
    if (typeof(hero_region) != 'undefined' && hero_region != null){
      hero_region[0].style.backgroundImage = "url('" + hero[0].getAttribute('data-hero-image') + "')";
    }
  }
}
